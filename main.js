const vkQr = require('@vkontakte/vk-qr');
const cheerio = require('cheerio');
const fs = require('fs');
const text2png = require('text2png');
const sharp	 = require('sharp');
const { convertFile }  = require('convert-svg-to-png');
const uuidv1 = require('uuid/v1');
const rimraf = require("rimraf");

const color_black	 = "#000000";
const color_white	 = "#ffffff";
const color_orange	 = "#f08a03";
const color_blue	 = "#433f95";

createBanner({
	bigPercent:"86%", 
	referPercent:"29%", link:"ldqr.ru/43/7", 
	companyName: "Андрей лох"
})

async function createBanner(banner_data){
	let uuid = await uuidv1();
	await fs.mkdirSync("./raw/" + uuid);
	await fs.mkdirSync("./output/" + uuid);
	await pngtext(banner_data.bigPercent, {filename: "bigPercent", fontsize: "450px", color: color_blue, path: "./raw/" + uuid + '/'});
	await pngtext(banner_data.link, {filename: "link", fontsize: "100px", color: color_blue, path: "./raw/" + uuid + '/'});
	await pngtext(banner_data.referPercent, {filename: "referPercent", fontsize: "370px", color: color_blue, path: "./raw/" + uuid + '/'});
	await pngtext(banner_data.companyName, {filename: "companyName", fontsize: "200px", color: color_orange, path: "./raw/" + uuid + '/'});
	await qrCode(banner_data.link, {filename: "qr_clink", path: "./raw/" + uuid + '/'})
	await svgToPng(uuid);
	await mergeImages(uuid);
	rimraf.sync("./raw/" + uuid + '/');
	console.log('./output/' + uuid + '/' + 'MAINMERGE.png');
	return './output/' + uuid + '/' + 'MAINMERGE.png';
}

function pngtext(payload, params = {}){
	
	let { fontTTF = "Gilroy", fontsize = "700px", filename = "leader", color = color_black , path = "./raw/"}  = params;
	let font = fontsize + " " + fontTTF;

	fs.writeFileSync(path + filename + '.png', text2png(payload, {
		font: font,
		localFontPath: './src/Gilroy-Regular.ttf',
		localFontName: fontTTF,
		color: color
	}));
}

async function qrCode(payload, params = {}){

	let { maincolor = color_black, markercolor = color_orange, size = 1000, filename = "leader", backgroundColor = color_white , path = "./raw/"}  = params;

	const qrSvg = vkQr.createQR(payload, {
		className: "leader",  
		qrSize: size,
		isShowLogo: false,
		backgroundColor: backgroundColor,
		isShowBackground: true,
		foregroundColor: maincolor,
	});

	const $ = cheerio.load(qrSvg,{
		xmlMode: true
	});

	$('path[id="point-0"]').attr('fill', markercolor);

	let stream = fs.createWriteStream( path + filename + ".svg");
		stream.once('open', function(fd) {
		stream.write($.xml());
		stream.end();
	});
} 

async function svgToPng(uuid){
	const inputFilePath = './raw/' + uuid + '/' + 'qr_clink.svg';
	const outputFilePath = await convertFile(inputFilePath);
}

async function mergeImages(uuid){
	let bigPercent = {};
	let referPercent = {width:0, height:0};
	let link = {width:0, height:0};
	let qr_clink = {width:0, height:0};
	let companyName = {width:0, height:0};


	await sharp('./raw/' + uuid + '/' + 'referPercent.png')
		.resize({ width: 380 })
		.toFile('./raw/' + uuid + '/' + 'referPercentNEW.png');

	let textwidth = 0;	
	await sharp('./raw/' + uuid + '/' + 'companyName.png').metadata()
	.then(function(metadata) {
		textwidth = metadata.width;
	});

	let linkwidth = 0;	
	await sharp('./raw/' + uuid + '/' + 'link.png').metadata()
	.then(function(metadata) {
		linkwidth = metadata.width;
	});

	if(linkwidth > 800 ){
		await sharp('./raw/' + uuid  + '/' + 'link.png')
		.resize({ width: 800 })
		.toFile('./raw/' + uuid + '/' + 'linkNEW.png');
	}else{
		await sharp('./raw/' + uuid + '/' + 'link.png')
		.toFile('./raw/' + uuid + '/' + 'linkNEW.png');	
	}

	if(textwidth > 2100){
		await sharp('./raw/' + uuid + '/' + 'companyName.png')
		.resize({ width: 2100 })
		.toFile('./raw/' + uuid + '/' + 'companyNameNEW.png');
	}else{
		await sharp('./raw/' + uuid + '/' + 'companyName.png')
		.toFile('./raw/' + uuid + '/' + 'companyNameNEW.png');
	}

	await sharp('./raw/' + uuid + '/' + 'bigPercent.png').metadata()
	.then(function(metadata) {
		bigPercent = {
			width:metadata.width,
			height:metadata.height
		}
	});
	await sharp('./raw/' + uuid + '/' + 'referPercentNEW.png').metadata()
	.then(function(metadata) {
		referPercent.width = metadata.width;
		referPercent.heigth = metadata.height;
	});
	await sharp('./raw/' + uuid + '/' + 'linkNEW.png').metadata()
	.then(function(metadata) {
		link.width = metadata.width;
		link.heigth = metadata.height;
	});
	await sharp('./raw/' + uuid + '/' + 'qr_clink.png').metadata()
	.then(function(metadata) {
		qr_clink.width = metadata.width;
		qr_clink.heigth = metadata.height;
	});
	await sharp('./raw/' + uuid + '/' + 'companyNameNEW.png').metadata()
	.then(function(metadata) {
		companyName.width = metadata.width;
		companyName.heigth = metadata.height;
	});
	
	await sharp('./src/tent_light.png')
    .composite([
    	{
    		input: './raw/' + uuid + '/' + 'bigPercent.png', 
    		left: 490 - parseInt(bigPercent.width/2), 
    		top: 3950
    	},
    	{
    	
    		input: './raw/' + uuid + '/' + 'referPercentNEW.png', 
    		left: 1785 - parseInt(referPercent.width/2), 
    		top: 3740 - parseInt(referPercent.height/2)
    	},
    	{
    		input: './raw/' + uuid + '/' + 'linkNEW.png', 
    		left: 3150 - parseInt(link.width/2), 
    		top:2950
    	},
    	{
    		input: './raw/' + uuid + '/' + 'qr_clink.png', 
    		left: 3150 - parseInt(qr_clink.width/2), 
    		top:3305
    	},
    	{
    		input: './raw/' + uuid + '/' + 'companyNameNEW.png', 
    		left: 1250 - parseInt(companyName.width/2), 
    		top: 3200
    	}
    	])
    .toFile('./output/' + uuid + '/' + 'MAINMERGE.png');
}
